const http = require('http')
const hmr = require('node-hmr')

let app;

hmr(() => {
  app = require('./index.js')
}, { watchDir: './' })

const server = http.createServer((req, res) => app(req, res))

server.listen(3000)
