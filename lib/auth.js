const jwt = require('express-jwt')
const jwks = require('jwks-rsa')

const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwkUri: 'https://deifyed.eu.auth0.com/.well-known/jwks.json'
  }),
  audience: 'https://contacts.develish.net',
  issuer: 'https://deifyed.eu.auth0.com/',
  algorithms: ['RS256']
})

function validPrincipal(principal) {
  if (principal === 'julius')
    return true

  return false
}

const old = function(request, response, next) {
  if (!'authorization' in request.headers) return response.status(401).end()

  const principal = request.headers.authorization.split(' ')[1]

  if (!validPrincipal(principal))
    return response.status(401).end()

  request.principal = principal

  next()
}

module.exports = jwtCheck
