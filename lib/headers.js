module.exports = function(request, response, next) {
  response.header('Access-Control-Allow-Origin', 'http://localhost:8080')
  response.header('Access-Control-Allow-Methods', ['POST', 'PATCH', 'GET', 'DELETE'])
  response.header('Access-Control-Allow-Headers', ['Authorization', 'Content-Type'])

  if (request.method.toLowerCase() === 'options')
    return response.status(200).end()

  next()
}
