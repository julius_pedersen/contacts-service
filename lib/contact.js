class Contact {
  constructor(contact) {
    this.id = contact.id || ''
    this.first_name = contact.first_name || ''
    this.last_name = contact.last_name || ''
    this.phone_number = contact.phone_number || ''
  }
}

module.exports = Contact
