const uuid = require('uuid/v4')
const Contact = require('./contact')

const NAMESPACE = 'contacts'

function contactKey(principal, id) {
  return `${NAMESPACE}:${principal}:contact:${id}`
}
function contactsKey(principal) {
  return `${NAMESPACE}:${principal}:contacts`
}

function getContact(db, principal, id) {
  return new Promise((resolve, reject) => {
    db.hgetall(contactKey(principal, id), (err, contact) => {
      if (err) return reject(err)

      resolve(new Contact(contact))
    })
  })
}

function getAllContacts(db, principal) {
  return new Promise(async (resolve, reject) => {
    const key = `${NAMESPACE}:${principal}:contacts`

    db.lrange([key, 0, -1], async (err, ids) => {
      const contacts = await Promise.all(ids.map(id => getContact(db, principal, id)))
      const filteredContacts = []

      contacts.forEach(raw_contact => {
        const contact = new Contact(raw_contact)

        filteredContacts.push(contact)
      })

      resolve(filteredContacts)
    })
  })
}

function createContact(db, principal, contact) {
  return new Promise(resolve => {
    contact.id = uuid()

    db.hmset(contactKey(principal, contact.id), new Contact(contact))
    db.rpush(contactsKey(principal), contact.id)

    resolve(contact.id)
  })
}

function updateContact(db, principal, contact) {
  return new Promise(async resolve => {
    const oldContact = new Contact(await getContact(db, principal, contact.id))

    const updatedContact = Object.assign(oldContact, contact)
    db.hmset(contactKey(principal, contact.id), updatedContact)

    resolve()
  })
}

function deleteContact(db, principal, id) {
  return new Promise(async (resolve, reject) => {
    const key = contactsKey(principal)

    db.lrange([key, 0, -1], (err, ids) => {
      let idIndex = 0
      while(ids[idIndex] !== id) {
        if (idIndex >= ids.length) return reject()

        idIndex++
      }

      db.lrem([contactsKey(principal), idIndex, id], (err, result) => {
        if (err) return reject()

        db.del(contactKey(principal, id), (err, result) => {
          if (err) return reject(err)

          resolve(result)
        })
      })
    })
  })
}

module.exports = {
  getAll: getAllContacts,
  get: getContact,
  create: createContact,
  update: updateContact,
  delete: deleteContact
}
