const express = require('express')
const uuid = require('uuid/v4')
const uuid_validate = require('uuid-validate')

const authMiddleware = require('./lib/auth.js')
const headerMiddleware = require('./lib/headers.js')
const ContactManager = require('./lib/contact_manager.js')

const HOST = '0.0.0.0'
const PORT = 3000
const BASE_ENTRYPOINT = '/contacts'

const app = express()

app.use(headerMiddleware)
app.use(authMiddleware)
app.use(require('express-redis')())

app.use(express.json())

/*
 ** /contacts
 */
app.get(BASE_ENTRYPOINT, async (request, response) => {
  const contacts = await ContactManager.getAll(request.db, request.principal)

  response.json(contacts).end()
})
app.post(BASE_ENTRYPOINT, async (request, response) => {
  const contact = request.body

  if ('id' in contact)
    return response.status(400).end()
  
  const id = await ContactManager.create(request.db, request.principal, contact)

  response.json({ id }).end()
})

/*
 ** /contacts/:id
 */
app.get(BASE_ENTRYPOINT + '/:id', async (request, response) => {
  if (!uuid_validate(request.params.id, 4)) return response.status(400).end()

  try {
    const contact = await ContactManager.get(request.db, request.principal, request.params.id)
    response.json(contact).end()
  } catch(err) {
    response.status(500).end()
  }
})

app.patch(BASE_ENTRYPOINT + '/:id', async (request, response) => {
  const contact = request.body

  await ContactManager.update(request.db, request.principal, contact)

  response.status(200).json({ id: request.params.id }).end()
})

app.delete(BASE_ENTRYPOINT + '/:id', async (request, response) => {
  if (!uuid_validate(request.params.id, 4)) return response.status(400).end()

  await ContactManager.delete(request.db, request.principal, request.params.id)

  response.status(200).end()
})

module.exports = app
